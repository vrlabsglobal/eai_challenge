# EAI_challenge

Clone the respository from the given link. Download and install elasticsearch from https://www.elastic.co/downloads/elasticsearch

The default port for the Express server is 3000 and for the Elasticsearch is 9200

Navigate into the project folder and run the command
npm install

It installs all the dependencies

Now num the command 'node server.js' to run the server. Before that make sure that the elasticserch server is up and running
After this run the command 'npm run test' to run all the automated tests.

