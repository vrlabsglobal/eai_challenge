const express = require('express')
const config = require('../config')
const model = require('../models/model')
const url = require('url')

const router = express.Router();

const indexName = "address-book18";

// initial configuration for creating the data model
router.get('/', (req,res) => {
    config.indexExists(indexName).then(
        (result) => {
            if(!result) {
                config.initIndex(indexName).then(
                (result) => {
                    if(result.acknowledged) {
                            model.initMapping(indexName).then(
                            (result_1) => {
                                if(result_1.acknowledged) return res.status(200).json({msg:"Index with the given name successfully created"})
                            } 
                            ).catch((error) => {return res.status(500).json({msg: "An error has occurred", err: error})})
                        }
                    else return res.status(500).json({msg: "Error in creating a data model"}) 
                    }
                ).catch((error) => {return res.status(500).json({msg: "An error has occurred", err: error})})
            } else return res.status(500).json({msg: "Index with the given indexName already exists"})
        }
    ).catch((error) => {return res.status(500).json({msg: "An error has occurred", err: error})})
})

//Creating a new contact in the data model
router.post('/contact', (req,res) => {
    config.elasticClient.create({
        index: indexName,
        type: "contact",
        id: req.body.firstName + req.body.lastName,
        body: {
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            mobileNumber: req.body.mobileNumber,
            street: req.body.street,
            city: req.body.city,
            state: req.body.state,
            country: req.body.country,
            zip: req.body.zip
        }
    }, (error, result) => {
        if(error) {
            return res.status(409).json({msg:"An error occurred", err: error})
        } else {
            return res.status(201).json({msg:"New contact successfully created", obj: result})
        }
    })
})

//Getting the contact details according to the given query
router.get('/contact', (req,res) => {
    if(Object.keys(req.query).length === 0) {
        config.elasticClient.search({
            index: indexName,
            type: "contact",
            body: {
                query: {
                    match_all: {}
                }
            }
        }, (error, result) => {
            if(error) return res.status(400).json({msg:"No documents found for the search parameter", err: error})
            else return res.status(200).json({msg:"Search Results", obj: result})
        })
    }
    else {
        config.elasticClient.search({
            index: indexName,
            type: "contact",
            q: req.query.query,
            size: req.query.pageSize,
            from: (req.query.page-1)*req.query.pageSize
        }, (error, result) => {
            if(error) return res.status(400).json({msg:"No documents found for the search parameter", err: error})
            else return res.status(200).json({msg:"Search Results", obj: result})
        })
    }
})

//get the contact details of the given user
router.get('/contact/:name', (req,res) => {
    config.elasticClient.get({
        index: indexName,
        type: "contact",
        id: req.params.name
    }, (error, result) => {
        if(error) return res.status(404).json({msg:"A contact with the username does not exist", err: error})
        else res.status(200).json({msg: "User successfully retrived", obj: result})
    })
})

//update the contact details of the given user
router.put('/contact/:name', (req,res) => {
    config.elasticClient.update({
        index: indexName,
        type: "contact",
        id: req.params.name,
        body: {
            doc: {
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                email: req.body.email,
                mobileNumber: req.body.mobileNumber,
                street: req.body.street,
                city: req.body.city,
                state: req.body.state,
                country: req.body.country,
                zip: req.body.zip
            }
        }
    }, (error, result) => {
        if(error) res.status(404).json({msg: "Cannot update. A contact with the given name doesn't exist", err: error})
        else {
            if(req.params.name == req.body.firstName+req.body.lastName){
                res.status(200).json({msg: "User successfully updated", obj: result})
            }
            else {
                config.elasticClient.get({
                    index: indexName,
                    type: "contact",
                    id: req.body.firstName+req.body.lastName
                }, (error) => {
                    if(error) res.status(200).json({msg: "User successfully updated", obj: result})
                    else return res.status(409).json({msg:"Cannot update. A contact with the username already exists. Username has to be unique", err: error})
                })
            }
        }
    })
})

//delete the contact details of the given user
router.delete('/contact/:name', (req,res) => {
    config.elasticClient.delete({
        index: indexName,
        type: "contact",
        id: req.params.name
    }, (error, result) => {
        if(error) return res.status(404).json({msg:"Cannot delete. A contact with the username does not exist", err: error})
        else res.status(200).json({msg: "User successfully deleted", obj: result})
    })
})

module.exports = router;