let chai = require('chai')
let supertest = require('supertest')
let config = require('../config')

let api = supertest('http://localhost:3000')
let expect = chai.expect

describe('Contact', () => {
    it('Should create an index', (done) => {
        api.get('/').set('Accept', 'application/json').expect(200,done)
    })

    it('Should add a new contact to the Database', (done) => {
        api.post('/contact').set('Accept','application/json')
            .send({
                "firstName": "Vishwatej10",
                "lastName": "Anugu",
                "email": "vish@gmail.com",
                "mobileNumber": 6462297685,
                "street": "Health",
                "city": "Stony Brook",
                "state": "New york",
                "country": "US",
                "zip": "50001"
            }).expect(200).end((err,res) => {
                expect(res.body.obj.result).to.equal("created")
                done()
            })
    })

    it('Should add a new contact to the Database', (done) => {
        api.post('/contact').set('Accept','application/json')
            .send({
                "firstName": "Vishwatej11",
                "lastName": "Anugu",
                "email": "vish@gmail.com",
                "mobileNumber": 6462297685,
                "street": "Health",
                "city": "Stony Brook",
                "state": "New york",
                "country": "US",
                "zip": "50001"
            }).expect(200).end((err,res) => {
                expect(res.body.obj.result).to.equal("created")
                done()
            })
    })

    it('Should add a new contact to the Database', (done) => {
        api.post('/contact').set('Accept','application/json')
            .send({
                "firstName": "Vishwatej12",
                "lastName": "Anugu",
                "email": "vish@gmail.com",
                "mobileNumber": 6462297685,
                "street": "Health",
                "city": "Stony Brook",
                "state": "New york",
                "country": "US",
                "zip": "50001"
            }).expect(200).end((err,res) => {
                expect(res.body.obj.result).to.equal("created")
                done()
            })
    })

    it('Should add a new contact to the Database', (done) => {
        api.post('/contact').set('Accept','application/json')
            .send({
                "firstName": "Vishwatej13",
                "lastName": "Anugu",
                "email": "vish@gmail.com",
                "mobileNumber": 6462297685,
                "street": "Health",
                "city": "Stony Brook",
                "state": "New york",
                "country": "US",
                "zip": "50001"
            }).expect(200).end((err,res) => {
                expect(res.body.obj.result).to.equal("created")
                done()
            })
    })

    it('Should get a new contact from the Database with the given name', (done) => {
        api.get('/contact/Vishwatej10Anugu').set('Accept','application/json')
            .expect(200).end((err,res) => {
                expect(res.body.obj._source).to.deep.equal(
                    {
                        "firstName": "Vishwatej10",
                        "lastName": "Anugu",
                        "email": "vish@gmail.com",
                        "mobileNumber": 6462297685,
                        "street": "Health",
                        "city": "Stony Brook",
                        "state": "New york",
                        "country": "US",
                        "zip": "50001"
                    }
                )
                done()
            })
    })

    it('Should update the contact in the Database with the given name', (done) => {
        api.put('/contact/Vishwatej10Anugu').set('Accept','application/json')
            .send({
                "email": "vish@cmail.com",
                "mobileNumber": 6462297684,
            })
            .expect(200).end((err,res) => {
                expect(res.body.obj.result).to.equal("updated")
                done()
            })
    })

    it('Waiting for POST to complete', function(done){
        setTimeout(function(){
          console.log('waiting over.');
             done();
             }, 1900)
     })

    it('Should search the contacts according to the given pageSize and pages', (done) => {
        api.get('/contact?pageSize=1&page=1').set('Accept','application/json').expect(200)
            .end((err,res) => {
                expect(res.body.obj.hits.hits.length).to.equal(1)
                done()
        })
    })

    it('Should search the contacts according to the given pageSize and pages', (done) => {
        api.get('/contact?pageSize=1&page=1&query=Anugu').set('Accept','application/json').expect(200)
            .end((err,res) => {
                expect(res.body.obj.hits.hits.length).to.equal(1)
                for(let i=0; i< res.body.obj.hits.hits.length; i++) {
                    expect(res.body.obj.hits.hits[i]._source.lastName).to.equal("Anugu")
                }
                done()
        })
    })

    it('Should delete the contact from the Database with the given name', (done) => {
        api.delete('/contact/Vishwatej10Anugu').set('Accept','application/json')
            .expect(200).end((err,res) => {
                expect(res.body.obj.result).to.equal("deleted")
                done()
            })
    })
})