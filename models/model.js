//defining the data model with necessary constraints

const elasticsearch = require('elasticsearch');

const elasticClient = new elasticsearch.Client({
    host: 'localhost:9200',
    log: 'info'
});

let initMapping = (indexName) => {
    return elasticClient.indices.putMapping({
        index: indexName,
        type: "contact",
        body: {
            properties: {
                firstName: {type: "text"},
                lastName: {type: "text"},
                email: {type: "keyword"},
                mobileNumber: {type: "long"},
                street: {type: "text"},
                city: {type: "text"},
                state: {type: "text"},
                country: {type: "text"},
                zip: {type: "keyword"}
            },
            _meta: {
                firstName: {
                    notnull: true
                },
                lastName: {
                    notnull: true
                },
                email: {
                    notnull: true,
                    regexp: "^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$"
                },
                mobileNumber: {
                    notnull: true,
                    regexp: "^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$"
                },
                street: {
                    notnull: true
                },
                city: {
                    notnull: true
                },
                state: {
                    notnull: true
                },
                country: {
                    notnull: true
                },
                zip: {
                    notnull: true
                }
            }
        }
    });
}

exports.initMapping = initMapping;