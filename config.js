const elasticsearch = require('elasticsearch');

const elasticClient = new elasticsearch.Client({
    host: 'localhost:9200',
    log: 'info'
});

exports.elasticClient = elasticClient;

let initIndex = (indexName) => {
    return elasticClient.indices.create({
        index: indexName
    });
}
exports.initIndex = initIndex;

let indexExists = (indexName) => {
    return elasticClient.indices.exists({
        index: indexName
    });
}
exports.indexExists = indexExists;
