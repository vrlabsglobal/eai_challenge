const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./routes/contact');
const config = require('./config');

const app = express();

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json());
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE, OPTIONS');
    next();
});

config.elasticClient.ping({
    requestTimeout: 3000,
}, (error) => {
    if(error) {
        console.log("Elasticsearch server is down :(")
    } else{
        console.log("Elasticsearch server is up and running!");
    }
});

app.use('/', routes);

const port = process.env.PORT || 3000;
app.listen(port);

console.log(`Address book server running on localhost:${port}`);